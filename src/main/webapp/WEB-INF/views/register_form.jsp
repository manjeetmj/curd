<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
    <div align="center">
        <h2>User Registration</h2>
        <form action="processform" method="post">
           <div class="form-group">
    <label for="exampleInputEmail1">Name </label>
    <input type="name" class="form-control" 
    id="name" 
    aria-describedby="emailHelp" 
    placeholder="Enter name"
    name="name">
    
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" 
    class="form-control" 
    id="email" 
    aria-describedby="emailHelp" 
    placeholder="Enter email"
    name="email">
    
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Password </label>
    <input type="password" class="form-control" 
    id="password" aria-describedby="emailHelp"
     placeholder="Enter password"
     name="password">
    
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Profession </label>
    <input type="profession" class="form-control"
     id="profession" aria-describedby="emailHelp"
      placeholder="Enter profession"
      name="profession">
    
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Note </label>
    <input type="note" class="form-control"
     id="note" aria-describedby="emailHelp"
      placeholder="Enter note"
      name="note">
    
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</html>