package com.springcrud.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springcrud.entity.Book;

@Service
public class Bookserviceimpl implements Bookservice {

	@Autowired
	BookRepository bookRepository;

	// save operation

	@Override
	public Book saveBook(Book book) {
		return bookRepository.save(book);
	}
	// read book
	@Override
	public List<Book> readbook() {
		// TODO Auto-generated method stub
		return (List<Book>) bookRepository.findAll();
	}

	// update book

//	@Override
//	public Book updatebook(Book book, Integer id) {
//		// TODO Auto-generated method stub
//		return bookRepository.updateBook(id);
//	}
	@Override
	public String updateBook(Book book) {
		// TODO Auto-generated method stub
		Optional<Book> b1 = bookRepository.findById(book.getId());
		if (b1.isPresent() == true) {
			bookRepository.save(book);
			return "update";
		} else {
			return "not found";
		}

	}
//	@Override
//	public List<Book> searchBook(String author) {
//		// TODO Auto-generated method stub
//		return bookRepository.findbyAuthor(author);
//
//	}
	@Override
	public List<Book> searchBook(String author) {
		// TODO Auto-generated method stub
		return bookRepository.findByAuthor(author);
	}

//	@Override
//	public List<Book> searchBook(String author) {
//		// TODO Auto-generated method stub
//
//		return bookRepository.findbyAuthor(author);
//
//	//	return list;
//	}

}
