package com.springcrud.dao;

import java.util.List;

import com.springcrud.entity.Book;

public interface Bookservice {

//save operation
	
	 Book saveBook(Book book);
	 
	 //read book
	 
	 List<Book> readbook();
	 
	 //update book
	 
	 String updateBook(Book book);
	 
	 //search book
	 
	 List<Book> searchBook(String author);
	 
	 
	 
}
