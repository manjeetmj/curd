package com.springcrud.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springcrud.entity.Book;
@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {
//	List<Book> findByAuthor(String author);
//	public List<Book>  findbyAuthor(String author);
	
//	List<Book> findByPublisher(String publisher);
     List<Book> findByAuthor(String author);
}
