package com.springcrud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



import com.springcrud.dao.Bookserviceimpl;
import com.springcrud.entity.Book;



@RestController
public class Mycontroller {

	@Autowired
	Bookserviceimpl bookServiceimpl ;
	
	@RequestMapping("/save")
	public Book savebook (@RequestBody Book book ) {
		System.out.println("hello");
		return bookServiceimpl.saveBook(book);
	}
	
	
	@RequestMapping("/read")
	public List<Book> readbook(){
		
		return bookServiceimpl.readbook();
	}
	
	
	@RequestMapping("/update")
	public String updateBook(@RequestBody Book book) {
		return bookServiceimpl.updateBook(book);
		
	}
	@RequestMapping(value = "/search", method = RequestMethod.POST )
	public List<Book> searchBook(String author) {
		System.out.print(author);
		return bookServiceimpl.searchBook(author);
	}
}
